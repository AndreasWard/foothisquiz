﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Assets.Scripts;
using System.Threading;

/*
 * This file is subject to the terms and conditions specified in
 * 'LICENSE.txt', which is part of this project source code package.
 */

public class GameManager : MonoBehaviour
{
    public Question[] questions;
    private static List<Question> unansweredQuestions = new List<Question>();

    [SerializeField]
    private static List<Player> players = new List<Player>();

    private Question currentRandomQuestion = new Question();
    private static int currentRoundNoOfQuestions = 10; //could change to array or list if including multiple rounds
    private static List<Question> questionsForCurrentRound = new List<Question>(currentRoundNoOfQuestions);

    private SpriteRenderer spriteRenderer = new SpriteRenderer();

    [SerializeField]
    private Text uiQuestionText;

    [SerializeField]
    private Text[] uiPossibleAnswersText = new Text[4];

    [SerializeField]
    private GameObject[] uiPossibleAnswersButtons = new GameObject[4];

    private Sprite uiPlayerIcon;

    [SerializeField]
    private Text uiPlayerRankText;

    [SerializeField]
    private Text uiPlayerScoreText;

    [SerializeField]
    private float delayBetweenQuestions = 10f;

    [SerializeField]
    private Text questionCountdownTimerText;

    private float questionCountdownTimerValue;

    private bool hasUserAnsweredQuestion = false;

    private static int correctAnswerPoints = 100; //value award to player for correct answer

    private static bool hasChosenQuestionsForCurrentRound = false;

    private static int roundNumber = 1;

    private bool hasCreatedPlayer = false;

    private void OnEnable() //when script is activated (reactivated when being called again)
    {
        
    }
    private void Start() //called at start of each new scene
    {
        SetupGame();
    }

    public void Update()
    {
        uiPlayerRankText.text = players[0].rank.ToString();
        uiPlayerScoreText.text = "Score: " + players[0].currentScore.ToString();
        //spriteRenderer.sprite = players[0].Icon;

        if (hasUserAnsweredQuestion == false)
        {
            UpdateQuestionCountdownTimer();
        }
    }

    private void SetupGame()
    {
        //initialise variables (when script is called after first time)


        //resetting values for next question
        questionCountdownTimerValue = 10;
        questionCountdownTimerText.text = questionCountdownTimerValue.ToString();
        hasUserAnsweredQuestion = false;

        if (players.Count < 1) //stops more than 1 player being created (for now where only 1 player is required)
        {            
            players.Add(new Player());
            Debug.Log("Number of players in game: " + players.Count);
        }

        if (roundNumber >=2)
        {
            Debug.Log("This is the second round");
        }

        //players[0].Icon.sprite = uiPlayerIcon.sprite;

        //try
        //{
        //    //players[0].Icon.sprite = Resources.Load<Sprite>("shape008_style01_color12");
        //}
        //catch (System.NullReferenceException e)
        //{
        //    Debug.Log("Cannot load Player Icon: " + e);
        //    throw;
        //}

        //uiPlayerIcon.sprite = players[0].Icon.sprite;
        //players[0].Icon.transform.Find("Canvas").Find("PlayerStatsPanel").Find("PlayerIcon").GetComponentInChildren<SpriteRenderer>().sprite = uiPlayerIcon.sprite;

        spriteRenderer = GameObject.Find("Canvas").gameObject.transform.Find("PlayerStatsPanel").gameObject.transform.Find("PlayerIcon").GetComponent<SpriteRenderer>();
        //spriteRenderer.sprite = players[0].Icon;

        //GetComponent<SpriteRenderer>().sprite = players[0].Icon;

        if (unansweredQuestions == null || unansweredQuestions.Count == 0 || questionsForCurrentRound.Count == 0) //set new question of scene UI
        {
            unansweredQuestions = questions.ToList<Question>();
        }

        if (hasChosenQuestionsForCurrentRound == false) //stops OutOfMemoryException when ChooseQuestionsForCurrentRound() is continuously called when scene is reloaded (i.e. every time a question is answered, a for loop adds questions to the questionsForCurrentRound list, which shouldn't occur)
        {
            ChooseQuestionsForCurrentRound();
        }
        SetCurrentQuestion();
        //Debug.Log(currentRandomQuestion.value + " correct answer is: " + currentRandomQuestion.actualFact);
    }

    private void ChooseQuestionsForCurrentRound()
    {
        
                while (questionsForCurrentRound.Count < currentRoundNoOfQuestions)
                {
                    questionsForCurrentRound.Add(unansweredQuestions[Random.Range(0, unansweredQuestions.Count)]);
                    questionsForCurrentRound = questionsForCurrentRound.Distinct().ToList(); //removes duplicate questions randomly picked and returns list of unique questions (or loops until that is the case, where the count < currentRoundNoOfQuestions if duplicates questions are continuously chosen)
                }
            
        
        hasChosenQuestionsForCurrentRound = true;
    }
    private void SetCurrentQuestion()
    {
        //int randomQuestionIndex = Random.Range(0, unansweredQuestions.Count);
        int randomQuestionIndex = Random.Range(0, questionsForCurrentRound.Count);
        currentRandomQuestion = questionsForCurrentRound[randomQuestionIndex];

        uiQuestionText.text = currentRandomQuestion.value;

        Debug.Log("SELECTING QUESTION METHOD removing question" + randomQuestionIndex + " from list. Question is: " + questionsForCurrentRound.ElementAt(randomQuestionIndex).value);
        if (questionsForCurrentRound.Contains(currentRandomQuestion))
        {            
            questionsForCurrentRound.RemoveAt(randomQuestionIndex);
        }
        //Thread.Sleep(500); //introduce delay so that question can be removed if player answers question very quickly, removing the question step may be skipped
        Debug.Log("Number of questions left in list SELECTING QUESTION METHOD = " + questionsForCurrentRound.Count);


        for (int i = 0; i < currentRandomQuestion.possibleFacts.Length; i++)
        {
            uiPossibleAnswersText[i].text = currentRandomQuestion.possibleFacts[i];
        }
    }

    IEnumerator TransistionToNextQuestion()
    {
        //unansweredQuestions.Remove(currentRandomQuestion);
        Debug.Log("removing question from list... list size BEFORE REMOVING = " + questionsForCurrentRound.Count);
        if (questionsForCurrentRound.Contains(currentRandomQuestion))
        {
            Debug.Log("What the heck? The current question is still in the list... error!");
            questionsForCurrentRound.Remove(currentRandomQuestion);
        }

        yield return new WaitForSeconds(delayBetweenQuestions);

        Debug.Log("Number of questions left in list = " + questionsForCurrentRound.Count);
        if (questionsForCurrentRound.Count == 0)
        {
            //save scores (add to highscores?)
            Debug.Log("No more questions... load highscore screen!");

            //Debug.Log("players[0]" + players[0].CurrentScore);
            //GameFileSystem.SaveHighscoreAsJSON(players.ToArray());
            //GameFileSystem.SavePlayerBinary(players[0]);
            //GameFileSystem.SaveHighscoresAgain(players[0]);
            GameFileSystem.SaveHighscoresPlayerPrefs(players[0]);
            //GameFileSystem.LoadGame();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

            //resetting values
            players[0].currentScore = 0;
            hasChosenQuestionsForCurrentRound = false;
            roundNumber++; //increment this value for the next round
            Destroy(this);
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //restart current scene
        }

    }

    public void UserSelectsAnswer(int selectedAnswer)
    {
        Debug.Log(selectedAnswer.ToString());
        if (hasUserAnsweredQuestion == false)
        {
            if (currentRandomQuestion.actualFact == currentRandomQuestion.possibleFacts[selectedAnswer])
            {
                //Debug.Log(EventSystem.current.currentSelectedGameObject.name);
                uiPossibleAnswersButtons[selectedAnswer].transform.Find("Button Layer").gameObject.GetComponentInChildren<Image>().color = Color.green;
                //uiPossibleAnswersButtons[selectedAnswer].GetComponentInChildren<Image>().color = Color.green;
                UpdatePlayerScore();
                Debug.Log("Correct answer!");
                hasUserAnsweredQuestion = true;
            }
            else
            {
                uiPossibleAnswersButtons[selectedAnswer].transform.Find("Button Layer").gameObject.GetComponentInChildren<Image>().color = Color.red;
                Debug.Log("Incorrect answer");
                hasUserAnsweredQuestion = true;
            }
        }

        StartCoroutine(TransistionToNextQuestion());
    }
    private void UpdatePlayerScore()
    {
        
        if (players[0].currentXp + correctAnswerPoints >= players[0].NextRank[players[0].rank + 1])
        {
            players[0].rank++;
        }

        players[0].currentScore += correctAnswerPoints;
        players[0].currentXp += correctAnswerPoints;

        uiPlayerScoreText.text = "Score: " + players[0].currentScore.ToString();
    }
    private void UpdateQuestionCountdownTimer()
    {
        //Debug.Log("countdown timer before if stmt: " + questionCountdownTimerValue.ToString());
        if (questionCountdownTimerValue - Time.deltaTime >= 0)
        {
            questionCountdownTimerValue -= Time.deltaTime;
            questionCountdownTimerText.text = questionCountdownTimerValue.ToString();
        }
        else
        {
            StartCoroutine(TransistionToNextQuestion());
        }

        //Debug.Log("countdown timer after if stmt: " + questionCountdownTimerValue.ToString());
    }

    private void OnApplicationPause(bool applicationPaused)
    {
        if (applicationPaused)
        {
            //Game paused
            Debug.Log("Game Paused");
            //GameFileSystem.SaveHighscoreAsJSON(players[0]);
        }
        else
        {
            //Game resumed
            Debug.Log("Game Resumed");
            //GameFileSystem.LoadHighscoresFromJSON();
            //LOAD PLAYER DATA, NOT HIGHSCORES
        }
        //Save game

        //Load game
    }
    //private static void SaveGame() //MAY NOT WORK
    //{

    //    BinaryFormatter binaryFormatter = new BinaryFormatter();
    //    string filePath = Application.persistentDataPath + "/player.foothisquiz";
    //    FileStream fileStream = new FileStream(filePath, FileMode.Create);
    //    binaryFormatter.Serialize(fileStream, players);
    //    fileStream.Close();

    //    //string jsonPlayerData = JsonUtility.ToJson(players); //to JSON

    //    //PlayerPrefs.SetString("PlayerData", jsonPlayerData);
    //    //PlayerPrefs.Save();
    //    Debug.Log("Player data saved (Quiz scene)");
    //}

    //private static List<Player> LoadGame() //MAY NOT WORK
    //{
    //    string filePath = Application.persistentDataPath + "/player.foothisquiz";

    //    if (File.Exists(filePath))
    //    {
    //        BinaryFormatter binaryFormatter = new BinaryFormatter();
    //        FileStream fileStream = new FileStream(filePath, FileMode.Open);
    //        players[0] = binaryFormatter.Deserialize(fileStream) as Player; //only using first element of list as only 1 player is needed at this time of development
    //        fileStream.Close();

    //        Debug.Log("Player info (current score) loaded from file:");
    //        Debug.Log(players[0].CurrentScore);
    //        //for (int i = 0; i < players.Count; i++)
    //        //{
    //        //    Debug.Log("Player" + i);
    //        //    Debug.Log(players[i].CurrentScore);
    //        //}
    //        return players;
    //    }
    //    else
    //    {
    //        Debug.Log("File not found");
    //        return null;
    //    }
    //    //string jsonPlayerData = PlayerPrefs.GetString("PlayerData");

    //    //JsonUtility.FromJsonOverwrite(jsonPlayerData, players);

    //    //for (int i = 0; i < players.Count; i++)
    //    //{
    //    //    Debug.Log("player" + i + "'s (rank): " + players[i].Rank);
    //    //}
    //}

    private void SaveToHighscores()
    {
        string jsonPlayerData = JsonUtility.ToJson(players); //to JSON

        LoadFromHighscores();
        //add if currentscore > lowest highscore... then add to highscores
        PlayerPrefs.SetInt("PlayerHighscores", players[0].currentScore);
        //PlayerPrefs.Save();
    }

    private void LoadFromHighscores()
    {

    }

    IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
    }
}
