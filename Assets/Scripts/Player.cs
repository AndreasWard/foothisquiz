﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

/*
 * This file is subject to the terms and conditions specified in
 * 'LICENSE.txt', which is part of this project source code package.
 */

[Serializable]
public class Player
{
    [SerializeField]
    public string name;
    [SerializeField]
    public int rank;
    [SerializeField]
    public int currentScore;
    [SerializeField]
    public int currentXp;

    private int[] nextRank = {0, 0, 500, 1100, 2000, 3200, 4700, 6500, 8600, 10000, 11700};
    //private Sprite icon;

    public Player()
    {
        name = "You";
        rank = 1;
        currentScore = 0;
        currentXp = 0;
        //icon = Resources.Load<Sprite>("black_headphones");
    }

    //public string Name { get => name; set => name = value; }
    //public int Rank { get => rank; set => rank = value; }
    //public int CurrentScore { get => currentScore; set => currentScore = value; }
    //public int CurrentXP { get => currentXp; set => currentXp = value; }
    public int[] NextRank { get => nextRank; }
    //public Sprite Icon { get => icon; set => icon = value; }
}
