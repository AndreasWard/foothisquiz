﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * This file is subject to the terms and conditions specified in
 * 'LICENSE.txt', which is part of this project source code package.
 */

public class Highscores : MonoBehaviour
{
    private GameObject canvas;
    private Transform highscoreEntryContainer;
    private Transform highscoreEntryTemplate;

    private List<Player> highscoringPlayers;
    private PlayerList playerList;
    private static Dictionary<string, int> highscores;
    private static Dictionary<string, int> sortedHighscoresDesc;

    public static int noOfHighscoreEnteries = 0; //used for providing unique key for saving and loading highscores

    private void Start()
    {
        highscores = GameFileSystem.LoadHighscoresPlayerPrefs();
        sortedHighscoresDesc = highscores.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

        #region code finally working
        //foreach (var unsortedHighscores in highscores)
        //{
        //    Debug.Log("unsorted highscores");
        //    Debug.Log(unsortedHighscores.Key + ", " + unsortedHighscores.Value);
        //}

        //foreach (var sortedHighscores in sortedHighscoresDesc)
        //{
        //    Debug.Log("sorted highscores");
        //    Debug.Log(sortedHighscores.Key + ", " + sortedHighscores.Value);
        //}
        #endregion

        canvas = GameObject.Find("Canvas");
        Transform highscoreTable = canvas.transform.Find("Background").transform.Find("HighscoreTable");
        highscoreEntryContainer = highscoreTable.transform.Find("HighscoreEntryContainer");
        highscoreEntryTemplate = highscoreEntryContainer.transform.Find("HighscoreEntryTemplate");

        highscoreEntryTemplate.gameObject.SetActive(false);

        for (int i = 0; i < 10; i++)
        {
            Transform highscoreEntryTransform = Instantiate(highscoreEntryTemplate, highscoreEntryContainer);
            RectTransform highscoreEntryRectTransform = highscoreEntryTransform.GetComponent<RectTransform>();
            highscoreEntryRectTransform.anchoredPosition = new Vector2(0, -20f * i);
            highscoreEntryTransform.gameObject.SetActive(true);

            int rank = i + 1;
            string rankAsString = "";

            switch (rank)
            {
                case 1:
                    rankAsString = "1st";
                    break;

                case 2:
                    rankAsString = "2nd";
                    break;

                case 3:
                    rankAsString = "3rd";
                    break;

                default:
                    rankAsString = rank + "th";
                    break;
            }

            highscoreEntryTransform.Find("PositionText").GetComponent<Text>().text = rankAsString;
            highscoreEntryTransform.Find("NameText").GetComponent<Text>().text = sortedHighscoresDesc.Keys.ElementAt(i);
            highscoreEntryTransform.Find("ScoreText").GetComponent<Text>().text = sortedHighscoresDesc.Values.ElementAt(i).ToString();
        }
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void LoadFromHighscores()
    {
        //string jsonPlayerData = PlayerPrefs.GetString("PlayerHighscores");

        //highscores = PlayerPrefs.GetInt("PlayerHighscores", -1);

        //JsonUtility.FromJson("jsonPlayerData", players);

        //for (int i = 0; i < players.Count; i++)
        //{
        //    Debug.Log("player" + i + "'s (rank): " + players[i].Rank);
        //}
    }

    private void SortHighscoresInDescOrder()
    {        
        //IList<Player> players = highscoringPlayers;
        //highscoringPlayers = players.OrderByDescending(player => player.CurrentScore).ToList();  
    }

    //public static void addScore(Player thePlayer)
    //{
    //    if (highscoringPlayers.Count > 0) //list is not empty
    //    {
    //        highscoringPlayers.Add(thePlayer);
    //        var highscoringPlayersDescOrder = highscoringPlayers.OrderByDescending(player => player.CurrentScore);
    //        highscoringPlayers = (IList<Player>)highscoringPlayersDescOrder;
    //    }
    //    else
    //    {
    //        highscoringPlayers.Add(thePlayer);
    //    }
    //    //check if player highscore is > 10th element in THIS highscoringPlayers list
    //    //if so, 
    //}
}
