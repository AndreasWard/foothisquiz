﻿/*
 * This file is subject to the terms and conditions specified in
 * 'LICENSE.txt', which is part of this project source code package.
 */
 
[System.Serializable] //can store info within Unity (can change questions in Unity)
public class Question
{
    public string value;
    public string[] possibleFacts = new string[4];
    public string actualFact;
}
