﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * This file is subject to the terms and conditions specified in
 * 'LICENSE.txt', which is part of this project source code package.
 */

namespace Assets.Scripts
{
    [Serializable]
    public class PlayerList
    {
        public List<Player> players;
        //public string name;

        //public int rank;

        //public int currentScore;

        //public int currentXp;

        //public PlayerData(Player player)
        //{
        //    name = player.name;
        //    rank = player.rank;
        //    currentScore = player.currentScore;
        //    currentXp = player.currentXp;
        //}

    }
}
