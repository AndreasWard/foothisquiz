﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/*
 * This file is subject to the terms and conditions specified in
 * 'LICENSE.txt', which is part of this project source code package.
 */

namespace Assets.Scripts
{
    public static class GameFileSystem
    {
        //static List<Player> players = new List<Player>();
        static string filePath = Application.persistentDataPath + "/player.json";

        public static void SaveHighscoresPlayerPrefs(Player player)
        {
            if (PlayerPrefs.HasKey("noOfHighscoreEnteries"))
            {
                Debug.Log("latest highscore enteries value" + PlayerPrefs.GetInt("noOfHighscoreEnteries"));
                PlayerPrefs.SetInt(player.name + PlayerPrefs.GetInt("noOfHighscoreEnteries"), player.currentScore);
            }
            else
            {                
                PlayerPrefs.SetInt(player.name + Highscores.noOfHighscoreEnteries, player.currentScore);
            }
            
            PlayerPrefs.Save();
            Highscores.noOfHighscoreEnteries = PlayerPrefs.GetInt("noOfHighscoreEnteries");
            Highscores.noOfHighscoreEnteries++;
            //save noOfHighscoreEnteries to PlayerPrefs also... check if has been saved, if so, reference this when saving highscores instead of using var in name
            PlayerPrefs.SetInt("noOfHighscoreEnteries", Highscores.noOfHighscoreEnteries); //check that this has updated the existing key value and not created a new one
            PlayerPrefs.Save();
        }

        public static Dictionary<string, int> LoadHighscoresPlayerPrefs()
        {
            Dictionary<string, int> playerHighscores = new Dictionary<string, int>();

            for (int i = 0; i < Highscores.noOfHighscoreEnteries; i++)
            {
                playerHighscores.Add("You" + i, PlayerPrefs.GetInt("You" + i));
            }

            foreach (var highscores in playerHighscores)
            {
                Debug.Log(highscores.Key + ", " + highscores.Value);
            }
            return playerHighscores;
        }
        #region old code
        //public static void SaveHighscoresAgain(Player player)
        //{
        //    List<Player> listOfPlayers = new List<Player>();
        //    listOfPlayers.Add(player);

        //    var playerList = new PlayerList();
        //    playerList.players = listOfPlayers;

        //    var jsonResult = JsonUtility.ToJson(playerList, true);
        //    File.AppendAllText(filePath, jsonResult);
        //}

        //public static PlayerList LoadHighscoresAgain()
        //{

        //    using(StreamReader streamReader = new StreamReader(filePath))
        //    {
        //        string json = streamReader.ReadToEnd();
        //        var jsonResult = JsonUtility.FromJson<PlayerList>(json);
        //        return jsonResult;
        //    }             
        //}

        //public static void SavePlayerBinary(Player player)
        //{
        //    BinaryFormatter binaryFormatter = new BinaryFormatter();
        //    string path = Application.persistentDataPath + "/player.foothisquiz";
        //    FileStream fileStream = new FileStream(path, FileMode.Append);

        //    List<PlayerData> playerData = new List<PlayerData>();
        //    playerData.Add(new PlayerData(player));

        //    binaryFormatter.Serialize(fileStream, playerData);
        //    fileStream.Close();
        //}

        //public static List<PlayerData> LoadPlayerBinary()
        //{
        //    string path = Application.persistentDataPath + "/player.foothisquiz";

        //    if (File.Exists(path))
        //    {
        //        BinaryFormatter binaryFormatter = new BinaryFormatter();
        //        FileStream fileStream = new FileStream(path, FileMode.Open);

        //        List<PlayerData> playerData = binaryFormatter.Deserialize(fileStream) as List<PlayerData>;

        //        fileStream.Close();

        //        return playerData;
        //    }
        //    else
        //    {
        //        Debug.Log("Save file not found");
        //        return null;
        //    }
        //}

        public static void SaveHighscore(List<Player> playerHighscore)
        {
            //Debug.Log("playerHighscore to save" + playerHighscore.CurrentScore);
            string filePath = Application.persistentDataPath + "/player.foothisquiz";

            using (Stream fileStream = File.Open(filePath, FileMode.Append, FileAccess.Write))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();

                if (File.Exists(filePath))
                {
                    //FileStream fileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write);
                    binaryFormatter.Serialize(fileStream, playerHighscore);
                    fileStream.Close();
                }
                else
                {
                    //FileStream fileStream = new FileStream(filePath, FileMode.Create);
                    binaryFormatter.Serialize(fileStream, playerHighscore);
                    fileStream.Close();
                }
            }
            //players = LoadHighscores();
            //players.Add(playerHighscore);

            //string jsonPlayerData = JsonUtility.ToJson(players); //to JSON

            //PlayerPrefs.SetString("PlayerData", jsonPlayerData);
            //PlayerPrefs.Save();
            Debug.Log("Player data saved (Quiz scene)");
        }

        public static List<Player> LoadHighscores()
        {
            string filePath = Application.persistentDataPath + "/player.foothisquiz";
            List<Player> tempPlayers;

            using (Stream fileStream = new FileStream(filePath, FileMode.Open))
            {
                if (File.Exists(filePath))
                {                    
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    //FileStream fileStream = new FileStream(filePath, FileMode.Open);

                    
                        //tempPlayers.Add(new Player());
                        tempPlayers= binaryFormatter.Deserialize(fileStream) as List<Player>; //only using first element of list as only 1 player is needed at this time of development
                    

                    fileStream.Close();

                    for (int i = 0; i < tempPlayers.Count; i++)
                    {
                        Debug.Log("Player info (current score) loaded from file:");
                        Debug.Log(tempPlayers[i].currentScore);
                    }

                    //for (int i = 0; i < players.Count; i++)
                    //{
                    //    Debug.Log("Player" + i);
                    //    Debug.Log(players[i].CurrentScore);
                    //}
                    return tempPlayers;
                }
                else
                {
                    Debug.Log("File not found");
                    return null;
                }
            }
            //string jsonPlayerData = PlayerPrefs.GetString("PlayerData");

            //JsonUtility.FromJsonOverwrite(jsonPlayerData, players);

            //for (int i = 0; i < players.Count; i++)
            //{
            //    Debug.Log("player" + i + "'s (rank): " + players[i].Rank);
            //}
        }

        public static void SaveHighscoreAsJSON(Player[] playerHighscore)
        {
            string thePlayerHighscore = JsonHelper.ToJson(playerHighscore, true);
            Debug.Log("NEW CODE... PLAYERHIGHSCORES");
            Debug.Log(thePlayerHighscore);


        //    using (StreamWriter streamWriter = new StreamWriter(filePath))
        //    {
        //        string json = JsonUtility.ToJson(playerHighscore, true);
        //        streamWriter.Write(json);
        //    }



            //Player theHighscore = new Player();
            //theHighscore = playerHighscore[0];
            //string jsonToSave = JsonUtility.ToJson(playerHighscore, true);

            //File.AppendAllText(filePath, jsonToSave);
            //Debug.Log("Saved player highscore: " + playerHighscore.CurrentScore + " to JSON");
        }

        public static Player[] LoadHighscoresFromJSON()
        {
            Player[] playerHighscores;
            //PlayerList playerList = new PlayerList();





            using (StreamReader streamReader = new StreamReader(filePath))
            {
                string json = streamReader.ReadToEnd();
                playerHighscores = JsonHelper.FromJson<Player>(json);
                Debug.Log("0: " + playerHighscores[0]);
                Debug.Log("1: " + playerHighscores[1]);
                //JsonUtility.FromJsonOverwrite(json, playerList);
            }
            //Debug.Log("player highscores loaded = " + playerHighscores[0].CurrentScore);
            
            //List<Player> thePlayers;
            //string dataFromJSON = File.ReadAllText(filePath);
            //Debug.Log("dataFromJSON");
            //Debug.Log(dataFromJSON);

            //thePlayers = JsonUtility.FromJson<List<Player>>(dataFromJSON);
            //Debug.Log("thePlayers" + thePlayers);
            ////for (int i = 0; i < thePlayers.Count; i++)
            ////{
            ////    Debug.Log(i + ". Player highscore: " + thePlayers[i].CurrentScore);
            ////}

            //Debug.Log("Loaded player highscores from JSON");

            return playerHighscores;
        }
        public static void DeleteAllSavedPlayerData()
        {
            string filePath = Application.persistentDataPath + "/player.foothisquiz";

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                Debug.Log("File: " + filePath + "deleted!");
            }
        }
    }
    //public class JsonHelper
    //{
    //    public static T[] getJsonArray<T>(string json)
    //    {
    //        string newJson = "{ \"array\": " + json + "}";
    //        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
    //        return wrapper.array;
    //    }

    //    [Serializable]
    //    private class Wrapper<T>
    //    {
    //        public T[] array;
    //    }
    //}

    public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}
    #endregion
}
