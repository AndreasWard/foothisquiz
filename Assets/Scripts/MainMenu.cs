﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * This file is subject to the terms and conditions specified in
 * 'LICENSE.txt', which is part of this project source code package.
 */

public class MainMenu : MonoBehaviour
{
    public void StartQuiz()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
        Destroy(this);
    }
}
